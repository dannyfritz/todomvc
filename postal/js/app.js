var todoChannel = postal.channel('todo');
var $todo = $('#TodoMVC');

$('#createTodo').on('submit', function(event) {
	var $this = $todo.find('#createTodo');
	var $input = $this.find('input');
	var text = $input.val();
	if (_.isEmpty(text)) {
		return;
	}
	todoChannel.publish('todo.new', {text: text});
	$input.val('');
});

var todoNewSubscription = todoChannel.subscribe(
	'todo.new',
	function (data) {
		var $ul = $todo.find('ul');
		var $task = $('<li>')
			.appendTo($ul);
		$('<span>' + data.text + '</span>')
			.appendTo($task);
		$('<a href="#">x</a>')
			.on('click', function (event) {
				todoChannel.publish('todo.delete', $task);
			})
			.appendTo($task);
	}
);

var todoDeleteSubscription = todoChannel.subscribe(
	'todo.delete',
	function ($task) {
		$task.remove();
	}
);