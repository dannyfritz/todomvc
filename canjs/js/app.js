var Todo = can.Model.extend({
});

var Todos = can.Control({
	init: function(element, options) {
		this.todos = new Todo.List([
			new Todo({text: 'Walk the dog'}),
			new Todo({text: 'Buy all the milk in the store'}),
			new Todo({text: 'Listen to Gold Motel'})
		]);
		this.form = new can.Observe({
			text: ''
		});
		can.append(this.element, can.view('todo', new can.Map({
			todos: this.todos,
			form: this.form
		})));
	},
	'submit': function(element, event) {
		var newText = this.form.attr('text');
		if (_.isEmpty(newText)) {
			return;
		}
		this.todos.push(new Todo({text: newText}));
		this.form.attr('text', '');
	},
	'input keyup': function(element, event) {
		var newText = $(element).val();
		this.form.attr('text', newText);
	}
});

new Todos('#TodoMVC');