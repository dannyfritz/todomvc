Polymer('todo-mvc', {
	ready: function () {
	},
	createItem: function (event) {
		var item = event.detail;
		if (!item) {
			return;
		}
		this.items.push(item);
	},
	deleteItem: function (event) {
		var item = event.detail;
		if (!item) {
			return;
		}
		_.remove(this.items, item);
	},
	items: [
		{text: 'Walk the dog'},
		{text: 'Buy all the milk in the store'},
		{text: 'Listen to Gold Motel'}
	]
});