Polymer('todo-item', {
	ready: function() {},
	deleteItem: function (event) {
		event.preventDefault();
		this.fire('itemdelete', this.item);
	}
});
