Polymer('todo-input', {
	ready: function () {
		this.newText = '';
	},
	createItem: function (event) {
		event.preventDefault();
		this.fire('itemcreate', {text: this.newText});
		this.newText = '';
	}
});