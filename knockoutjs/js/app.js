function Todo(options) {
	this.text = options.text;
}

function TodoMVCViewModel() {
	var self = this;
	this.newTodoText = ko.observable('');
  this.todos = ko.observableArray([
  	new Todo({text: 'Walk the dog'}),
  	new Todo({text: 'Buy all the milk in the store'}),
  	new Todo({text: 'Listen to Gold Motel'})
  ]);
  this.createTodo = function newTodo() {
  	if (_.isEmpty(self.newTodoText())) {
  		return;
  	}
  	self.todos.push(new Todo({
  		text: self.newTodoText()
  	}));
  	self.newTodoText('');
  };
  this.removeTodo = function removeTodo(todoIndex) {
  	self.todos.remove(todoIndex);
  };
};

ko.applyBindings(new TodoMVCViewModel, $('#TodoMVC').get(0));